/** Localization Challenge
 * 
 * belief.h 
 *
 * Belief of Particle Filter 
 * See Chapter 4 of Thrun
 */

#ifndef _BELIEF_H_
#define _BELIEF_H_
#include "decay_weights.h"

struct belief
{
	std::vector<double> weights; // weights of belief
	std::vector<RobotState> posevec; // pose vector 	 
	size_t numpart;	 // number of particles

	// Clears the belief
	belief(): numpart(0) {clear();}
	
	// Initializes poses with copies of first pose, each with same weight
	belief(size_t numpart,RobotState initPose): numpart(numpart)
	{
		posevec.clear();
		weights.clear();
		for(size_t i = 0; i != numpart; ++i)
		{
			posevec.push_back(initPose);
			weights.push_back(1.0/numpart);
		}
	}

	// copy constructor	
	belief(const belief &other): numpart(other.numpart)
	{
		clear();
		posevec = std::vector<RobotState> (numpart);
		std::copy(other.posevec.begin(), other.posevec.end(), this->posevec.begin());

		weights = std::vector<double> (numpart);
		std::copy(other.weights.begin(), other.weights.end(), this->weights.begin());
		
	}

	// normalizes weights to sum to unity
	void normalize() 
	{
		::normalizeWeights(weights);
	}

	// Sets all weights equal, so they sum to unity
	void equalize()
	{	
		::equalizeWeights(weights,numpart);
	}
	
	
	// Returns mean of weight vector
	double findMeanWeight()
	{
		return ::getMeanWeight(weights);
	}

	// Clear all vectors
	void clear()
	{
		numpart = 0;
		weights.clear();
		posevec.clear();
		//posevec_land.clear();
		//weights_land.clear();
	}

	/*
	* Fill a fraction of current pose vector with samples from the landmark reading
	*/	
	void fillSampledPoses(MarkerObservation obs, FieldLocation marker, RobotParams params, size_t fill)
	{
		//size_t sampledPoses = posevec_land.size();
		assert (fill < numpart);
		double weight = 0.0;
		RobotState random_pose;
		double minweight = * (std::min_element(weights.begin(), weights.end()));
		for(size_t i = 0; i < fill ; ++i)
		{
			do
			{
			   random_pose = sampleLandmarkPose(obs, marker, params);
			   weight = findMeasWeight(random_pose,obs,marker,params);	
			} while(weight <= minweight);
			
			//std::cout<<"\nRandom pose = "<< random_pose.x <<" " << random_pose.y <<" " << random_pose.theta;
			//std::cout<<"\nRandom pose Weight = " << weight << "\t using landmark at " << marker.x << ", " << marker.y;				
			posevec.at(i) = random_pose;
			weights.at(i) = weight;
		}
		normalize();		

				
	}
	

	/*
	* Resample the current belief, using the observation given
	*/	
	belief resample(decay_weights w_struct, MarkerObservation obs, FieldLocation marker, RobotParams params)
	{
		
		belief resamp;
		resamp.clear();		
		resamp.numpart = numpart;
		
		//std::cout <<"this->weights.size() = "<< this->weights.size();
		//this->normalize();
		assert(weights.size() == numpart);	
		// std::cout<<"\nFinished normalizing";		
		// low variance resampling
		double rv = genUniRV() * 1.0 / double(numpart);
		double c = weights.at(0);
		// std::cout << "\nrv = " << rv <<"\t c = "<< c;		
		
		double random_pose_prob = w_struct.random_pose_prob();
		//resamp.sampleLandmarkPoses(obs, marker, params);
		

		size_t pos = 0;
		for(size_t i = 0; i != numpart; ++i)
		{
			double u = rv + double (i + 1) / numpart;
			while (u > c && ++pos < numpart)
			{
				c += weights.at(pos);
					
			}
			// std::cout << "\n pos = "<< pos;
		 	size_t newpos = (pos >= numpart) ? numpart - 1 : pos; 	
			resamp.posevec.push_back(posevec.at(newpos));
			resamp.weights.push_back(weights.at(newpos));	
		}
		//();
				
		assert(resamp.posevec.size() == numpart);

		
		resamp.equalize();
		return resamp;				
	}
	
	/* 
	* Compute weighted average of all poses, for estPose in main
	*/	
	RobotState getEstPose()
	{
		// Weighted average of all poses
	
		normalize();
		RobotState estPose = ::getMeanPose(posevec,weights);		
		std::cout << "\nestPose = "<< estPose.x << ", "<< estPose.y << "," << estPose.theta;
		return estPose;
			
	}

	~belief() { clear();}

};


#endif
