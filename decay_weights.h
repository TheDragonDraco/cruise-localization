/** Localization Challenge
 * 
 * decay_weights.h 
 *
 * Parameters that control rate of introduction of random poses for AMCL
 * See Table 8.3 of Thrun
 */

#ifndef _DECAY_WEIGHTS_H_
#define _DECAY_WEIGHTS_H_

#include "utils.h"


struct decay_weights
{
	double alpha_slow,alpha_fast; // alpha_slow = short-term likelihood coeff, alpha_fast = long_term likelihood coeff
	double w_slow, w_fast; // short and long term likelihood weights 
	double pmax, pmin; // Maximum and minimum probabilities of introducing random poses (not in Thrun)
	double w_avg; // avg weight
	bool initialized; // flag to set w_slow and w_fast
	

	
	decay_weights(double alpha_s = 0.001, double alpha_f = 0.1): alpha_slow(alpha_s), alpha_fast(alpha_f), w_avg(0.0), w_slow(0.0), w_fast(0.0), initialized(false)
	{

	}

	/*
	* Update weights as per Table 8.3 of Thrun
	*/
	void update_weights(double w_avg2)
	{
		//std::cout << "\nw_avg = "<< w_avg2;		
		w_avg = w_avg2;
		
		if (!initialized)
		{
			w_slow = w_fast = w_avg;
			initialized = true;
		}
		else
		{							
			w_slow += alpha_slow * (w_avg - w_slow);		
			w_fast += alpha_fast * (w_avg - w_fast);
		}
		//std::cout << "\tw_slow = "<< w_slow <<"\tw_fast=" << w_fast;
	}

	/*
        * Get the probability of introducing a random pose. Same as Thrun, *except* ours is limited to [pmin,pmax] if positive
	*/
	double random_pose_prob()
	{
		double prob =  1.0 - w_fast / w_slow;
		if (prob < 0)
			return 0.0;
		else return clamp(prob, pmin, pmax);
		
	}
};


#endif
