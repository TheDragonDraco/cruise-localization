/** Localization Challenge
 * 
 * PF.h 
 *
 * Implements Augmented Monte Carlo Localization 
 * See Table 8.3 of Thrun
 */


#ifndef _PF_H_
#define _PF_H_
#include "RobotDefinitions.h"
#include "decay_weights.h"
#include "belief.h"
#include <iostream>
#include <algorithm>
#include <functional>
#include <numeric>
#include <cassert>


/*
* Particle filter class
*/
class PF 
{
	private:
	size_t numpart; // number of particles
	
	RobotState initPose; // initial pose
	RobotParams robotParams; // sensor params
	FieldLocation markers[NUM_LANDMARKS]; // landmarks
	std::vector<MarkerObservation> latestObs; // observation

	// Beliefs
	belief initBel; // initial belief
	belief priMot, postMot, postRes; // prior to motion step, post motion step (also used for measurement), and post resampling
	belief *latest;	// latest belief


	// Timestamps 
	size_t ts, ts_last_resamp, resamp_interval; // current timestamp, timestamp of last resampling, and interval between consecutive resamplings
	
	decay_weights w_struct;	// weights required for random poses
	double sd_thresh; // threshold SD of particles required for resampling
	
	public:
        
	// Constructor does nothing
        PF(): numpart(0) {}
		
	// Initializer
	void init (size_t numpart, const RobotState &initPose, RobotParams robotParams, FieldLocation markvec[], decay_weights w_struct2, double sd_thresh = 0.001,  size_t resamp_interval = 100)
	{ 
		this->numpart = numpart;
		this->robotParams = robotParams;
		this->w_struct = w_struct2;
		this->ts = this->ts_last_resamp = 0;
		this->sd_thresh = sd_thresh;
		this->resamp_interval = resamp_interval;		

		std::cout << "\nMarkers:";

		for (size_t i = 0; i < NUM_LANDMARKS; ++i)
		{
			this->markers[i] = markvec[i];
			std::cout << "\n" << markvec[i].x <<" , "<< markvec[i].y; 
		}

		std::cout<< "Initial pose :"<< initPose.x <<" , "<< initPose.y <<" , " << initPose.theta; 

		// Set initial belief
		std::cout << "Initializing pose:";
		initBel = belief(numpart,initPose);
		latest = &initBel;

	}

	
	/*
	* Samples motion model corresponding to delta. See Table 5.6 of Thrun
	*/
	void sampleMotionModel(RobotState delta)
	{
		priMot = *latest;		

		
		//std::cout<<"Delta = " << delta.x <<", " << delta.y << "," << delta.theta << "\n";
		
		postMot.clear();
		postMot.numpart = priMot.numpart;
	
		std::vector <RobotState>:: const_iterator i = priMot.posevec.begin();		
		std::vector <double> ::const_iterator j = priMot.weights.begin();		

		for (; i != priMot.posevec.end(); ++i,++j)
		{	
			RobotState newPose = sampleMotionModel(*i, delta);
			// std::cout<<"\nMoved from :" << i->x <<"," << i->y <<"," << i->theta <<"\t to "<< newPose.x <<"," << newPose.y << "," << newPose.theta;
			postMot.posevec.push_back(newPose);
			postMot.weights.push_back(*j);
		}
	
		postMot.normalize();
	
		latest = &postMot;
		
		++ts;
		latestObs.clear();

	}

	/*
	 * See table 5.6 of Thrun
	*/
	RobotState sampleMotionModel(RobotState const origPose, RobotState delta) 
	{
		double alpha1 = robotParams.odom_noise_rotation_from_rotation;
		double alpha2 = robotParams.odom_noise_rotation_from_translation;
		double alpha3 = robotParams.odom_noise_translation_from_translation;
		double alpha4 = robotParams.odom_noise_translation_from_rotation;

		assert ( fabs (delta.y) < 0.001 ); // sanity check for odometry

		double delta_rot1 = delta.theta, delta_rot1_sq = delta_rot1 * delta_rot1;
		double delta_rot2 = 0.0 /* angleDiff(delta.theta , delta_rot1)*/, delta_rot2_sq = 0.0;
		double delta_trans_sq = delta.x * delta.x;
		double delta_trans = delta.x;

		//std::cout << "\nOrig pose = " << origPose.x <<"  " << origPose.y <<"  " << origPose.theta;
		//std::cout << "\nDelta_rot1 = " << delta_rot1 << "  delta_trans = " << delta_trans << "\n" ;
 
		double stdev1 =  sqrt(alpha1 * (delta_rot1_sq) + alpha2 * (delta_trans_sq));	
		double theta1_est = angleDiff(delta_rot1 , genNormalRV(0, stdev1));
		
		double stdev2 = sqrt(alpha3 * delta_trans_sq + alpha4 * (delta_rot1_sq + delta_rot2_sq ));
		double trans_est = delta_trans - genNormalRV(0,stdev2);

		// Compute new positions 
		double mx = origPose.x + trans_est * cos(origPose.theta + theta1_est);
		double my = origPose.y + trans_est * sin(origPose.theta + theta1_est);
		// std::cout << "\ntheta1_est = " << theta1_est <<"  trans_est = "<< trans_est;


		RobotState movedPose = {mx, my , (origPose.theta + theta1_est )   } ;
		return normalizePose(movedPose);		
		
	}

	/*
         * Measurement model in Table 6.4 of Thrun
	*/
	void makeMeasurement(std::vector<MarkerObservation> obsvec)
	{

		if (obsvec.size() != 1)  
		{
			std::cout << "\nGot obsvec of size != 1";		
			return;
		}

		latestObs.clear(); 			
		latestObs.push_back(obsvec.at(0));
				
		FieldLocation marker = markers[latestObs.at(0).markerIndex];
		// std::cout << "\nMarker = " << marker.x << "," << marker.y;
		size_t count = 0;
		//std::cout << "\n numpart = "<< postMot.posevec.size() << " weights = "<< postMot.weights.size();
		postMot.weights.clear();
		
		for (std::vector<RobotState> ::iterator i = postMot.posevec.begin(); i != postMot.posevec.end(); ++i)
		{	
			double weight = findMeasWeight(*i, latestObs.at(0), marker, robotParams);
			//std::cout <<" count = " << count;			
			postMot.weights.push_back(weight);
				
		}
		 		

		// Update weight structure 
		w_struct.update_weights(postMot.findMeanWeight());
		double prob = w_struct.random_pose_prob();
		
		//std::vector<double> ::iterator maxelem = std::max_element(postMot.weights.begin(),postMot.weights.end());
		//size_t index = maxelem - postMot.weights.begin();
		//RobotState maxpose = postMot.posevec.at(index);
	
		//std::cout<< "\nPosition of elem with max weight = "<<  maxpose.x <<"," << maxpose.y <<"," << maxpose.theta;

		// If prob > 0, then introduce random poses
		if (prob > 0)
		{			
			size_t count = 	(size_t) (prob * numpart);
			std::cout <<"\nprob=" <<  prob << "\tFilling  "<< count <<" random poses\n";
			postMot.fillSampledPoses(latestObs.at(0), marker, robotParams, count);
		}
	
		postMot.normalize();  		
		latest = &postMot;

		double SD = getSD(postMot.weights);
		size_t time_since_resamp = ts - ts_last_resamp;

		// If SD exceeds threshold, and resamp_interval is nonzero, and we've not resampled for that many steps, then resample
		if (SD >= sd_thresh && resamp_interval && time_since_resamp > resamp_interval) 
		{
			std::cout<<"\n SD = "<< SD << " resampling \n ";
			postRes = postMot.resample(w_struct, latestObs.at(0),marker,robotParams);	
			std::cout << "\n new SD = " << getSD(postRes.weights); 	
			//double disc = postRes.getDiscEstPoses();
			latest = &postRes;
			ts_last_resamp = ts;
		}

	}

		
	
	RobotState getEstPose()
	{
		return latest->getEstPose();
	}	

	
	
	std::vector<RobotState>& getParticles()
	{
		return latest->posevec;
	}


};

#endif 




