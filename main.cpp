/**
 * Localization Challenge
 * 
 * main.cpp 
 *
 * Initiates program main loop, contains function templates for 
 * localization procedures to be implemented.
 */

#include <stdio.h>
#include <stdlib.h>
#include <GL/gl.h>
#include <GL/glu.h>
#include <GL/glut.h>
#include <math.h>
#include <vector>
#include "RobotDefinitions.h"
#include "LocalizationController.h"
#include "main.h"




/**
 * getRobotPositionEstimate()
 * This function is called by the controller to retrieve the current 
 * robot position estimate. 
 */
void getRobotPositionEstimate(RobotState& estimatePosn)
{
    // TODO: Write your procedures to set the current robot position estimate here
    estimatePosn = myPF.getEstPose();	
    //std::cout <<" estimated Position = "<< estimatePosn.x << "," << estimatePosn.y << "," << estimatePosn.theta << '\n';
}

/**
 * motionUpdate()
 * This function is called every time the position of the robot is
 * updated. The argument passed is the relative change in position of the 
 * robot in local robot coordinates (observed by odometry model), which 
 * may be subject to noise (according to motion model parameters).
 */
void motionUpdate(RobotState delta)
{
    // TODO: Write your motion update procedures here
    myPF.sampleMotionModel(delta);    	
}

/**
 * sensorUpdate()
 * This function is called every time the robot detects one or more
 * landmarks in its field of view. The argument passed contains all 
 * marker obervations (marker index and position of marker in robot 
 * coordinates) for the current frame.
 */
void sensorUpdate(std::vector<MarkerObservation> observations)
{
    // TODO: Write your sensor update procedures here
     myPF.makeMeasurement(observations);
}

/**
 * myinit()
 * Initialization function that takes as input the initial 
 * robot state (position and orientation), and the locations
 * of each landmark (global x,y coordinates).
 */
void myinit(RobotState robotState, RobotParams robotParams, 
            FieldLocation markerLocations[NUM_LANDMARKS])
{
    // TODO: Write your initialization procedures here
    decay_weights w_struct;
    size_t numpart = 500; 
    w_struct.pmax = 1.0 / numpart;
    w_struct.pmin = 0.01 / numpart;
	   			
    srand(time(NULL));
			
    myPF.init(numpart,robotState, robotParams,markerLocations, w_struct, 0.1, 30);
  
}

/**
 * mydisplay()
 * This function is called whenever the display is updated. The controller
 * will draw the estimated robot position after this function returns.
 */
void mydisplay()
{
    // TODO: Write your drawing procedures here 
    //       (e.g., robot position uncertainty representation)
	    
     std::vector<RobotState> &pvec = myPF.getParticles();
		
    
//    // Example drawing procedure
     int pixelX, pixelY;
//    double globalX = 1.0, globalY = -1.0;
//    const int NUM_POINTS = 8;
//    const double POINT_SPREAD = 0.2;
//    
//     Draw cyan colored points at specified global locations on field
     glBegin(GL_POINTS);
     glColor3f(0.0, 1.0, 2.0);

     for(size_t i = 0; i< pvec.size(); i++)
	{
        	global2pixel(pvec.at(i).x , pvec.at(i).y, pixelX, pixelY);
        	glVertex2i(pixelX, pixelY);
    	}

 
    glEnd();

}

/**
 * mykeyboard()
 * This function is called whenever a keyboard key is pressed, after
 * the controller has processed the input. It receives the ASCII value 
 * of the key that was pressed.
 *
 * Return value: 1 if window re-draw requested, 0 otherwise
 */
int mykeyboard(unsigned char key)
{
    // TODO: (Optional) Write your keyboard input handling procedures here
	
	return 0;
}


/**
 * Main entrypoint for the program.
 */
int main (int argc, char ** argv)
{
    // Initialize world, sets initial robot position
    // calls myinit() before returning
    runMainLoop(argc, argv);

    return 0;
}

