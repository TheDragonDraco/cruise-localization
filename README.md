Overview of solution
----

I implemented the Augmented Monte Carlo Localization (AMCL) Algorithm  (Prob. Robotics, Chap.8) for the solution.  


Files
---

* PF.h : particle filter class. 

Each routine in main.cpp calls a single PF member function.
The AMCL-related routines are all implemented here. Beliefs are stored in the belief data structure, below.

1. sampleMotionModel: samples the motion model of particles, using odometry
2. makeMeasurements: computes probabilities corresponding to measurements
3. getEstPose: get estimated pose 


* belief.h : belief structure with a vector of poses and weights. 

1. resample() : Low variance resampler for particle filter weights [Thrun, Chap 4]
2. getEstPose() : gets the estimated pose of current belief

* decay_weights.h : Implements weights for controlling the rate at which sample poses are introduced.

* utils.h : utilty functions for the rest of the code.



Performance
----

The performance depends on the parameters used to initialize the PF class:

1. Weight parmaters alpha_fast and alpha_slow of the weight_struct (set to 0.1 and 0.001 by default following ROS AMCL), and . These control the rate of introduction of random poses to make AMCL robust to kidnapping. Also, the max and min probabilities pmax and pmin of the weight struct that limit this probability.

2. Whether resampling is done at all (by setting resamp_time_interval =0 , no resampling is done) ; if so, the threshold SD and the resampling interval (time between consecutive resamplings). 

Here are qualitative observations from the parameter settings:


No resampling:			 more diversity in particles, and smooth pose tracking 	       VS   	slower kidnap recovery
Occasional resampling:		 less diversity in particles, and jerky pose tracking	       VS    	faster kidnap recovery 	 
Larger range of [pmin,pmax]:	 More random poses introduced against kidnapping	       VS 	localization skewed by random poses 
Smaller range of [pmin,pmax]:	  Less random poses leading to poor kidnap recovery	       VS	smoother pose tracking

3. Relaxations:
 * When no sensor updates are made, the tracking obviously fails after a while
 * When no random poses are introduced, kidnap recovery occurs only by chance. It's a small field, so the estimated and the *actual* pose (from your localization controller) will be close after a while.
 It's hard to distinguish between the performance of this vis-a-vis that of kidnap recovery.







