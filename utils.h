/** Localization Challenge
 * 
 * utils.h 
 *
 * Utility functions for belief and particle filter
 * 
 */


#ifndef _UTILS_H_
#define _UTILS_H_

#include <cmath>
#include <cstdlib>
#include <cstdio>
#include <ctime>
#include <vector>
#include <algorithm>
#include <functional>
#include <numeric>
#include <cassert>
#include <iostream>


/**
 * Clamps argument x between the lower bound l and upper bound u 
 * so that return value lies in [l,u]  
 */
double clamp(double x, double l, double u)
{
	return std::max(std::min(x,u),l);
}


/** 
 * Generates uniform RV in [0,1]
 */
double genUniRV()
{
	return (double)(rand())/ (double)(RAND_MAX);
}

/**
 * Generates Normal RV with prescribed mean and SD
 */
double genNormalRV(double mean, double stdev)
{
	double u1 = genUniRV(), u2 = genUniRV();
	double v1 = sqrt(-2 * log(u1)) * cos (2 * M_PI * u2);
	return v1 * stdev + mean;
}

/**
 * Computes f(x) where f is the normal pdf with given mean and SD 
*/
double findProbNormal(double x, double mean, double stdev)
{
	double var = stdev * stdev;		
	return  exp(-(x - mean) * (x - mean) / (2 * var)) / (stdev * sqrt(2 * M_PI)) ;		
}

/**
 * Normalizes angle theta to [-PI,+PI]
*/
double normalize(double theta)
{
	return atan2(sin(theta), cos(theta));	
}

/*
 * Ensures a robot pose falls within field 
*/
RobotState normalizePose(RobotState pose)
{
	double maxx = 2.6 , maxy = 2.0; // noted from practice	

	RobotState pose2 = { clamp (pose.x, -maxx, +maxx), 
			     clamp (pose.y, -maxy, +maxy), 
			     normalize(pose.theta)
			    };
	return pose2;
}

/* ---  Weight vector functions ----*/

/*
 * Normalizes a vector so it sums to unity
*/
void normalizeWeights(std::vector<double> &weights)
{
	std::cout << "\nweights.size() = "<< weights.size();	
	assert(weights.size() > 0);
	double weightsum = std::accumulate(weights.begin(), weights.end(), 0.0);
	std::cout <<"\n weightsum = "<< weightsum;	
	for (std::vector<double> ::iterator i = weights.begin(); i != weights.end(); ++i)
		*i /= weightsum;

	//std::cout << "\n sum = " << std::accumulate(weights.begin(), weights.end(),0);
	//assert(fabs(std::accumulate(weights.begin(), weights.end(),0) - 1.0) < 0.1 );		
}

/*
 * Fills in a weight vector with equal weights that sum to unity
*/
void equalizeWeights(std::vector<double> &weights, double length)
{	
	double value = weights.size()? 1.0/weights.size() : 1.0/length;
	weights.clear();	
	// std::cout <<"\nEqualizing weights to : "<< value;	
	for(size_t i = 0; i != length; ++i)
		weights.push_back(value);
}

/*
 * Mean of vector
*/
double getMeanWeight(std::vector<double> &weights)
{
	// std::cout<<"\n Finding mean weight of  " << weights.size();	
	assert (weights.size() > 0);
	return std::accumulate(weights.begin(), weights.end(), 0.0) / (double) weights.size();	
}

/*
 * SD of vector
*/ 
double getSD(std::vector<double> &weights)
{
	double mean = getMeanWeight(weights);
	double sq_sum = std::inner_product(weights.begin(), weights.end(), weights.begin(), 0.0);
	return sqrt(sq_sum / weights.size() - mean * mean);
}

/*
 * Get distance between positions of two poses
*/
double getDist(RobotState pose1, RobotState pose2)
{
	double dx = pose1.x - pose2.x, dy = pose1.y - pose2.y;
	return sqrt(dx * dx + dy * dy);
}

/*
 * Get weighted mean of array of poses
*/
RobotState getMeanPose(std::vector<RobotState> &posevec, std::vector<double> &weights)
{
	RobotState estpose = {0.0,0.0,0.0};
	size_t numpart = posevec.size();
	assert(weights.size() == numpart);		

	for (size_t i = 0; i < numpart; ++i)
	{
		estpose.x += weights.at(i) * posevec.at(i).x; //estpose.x = clamp(estpose.x, -FIELD_LENGTH,+FIELD_LENGTH);
		estpose.y += weights.at(i) * posevec.at(i).y; //estpose.y = clamp(estpose.y, -FIELD_WIDTH, +FIELD_WIDTH);
		estpose.theta += weights.at(i) * posevec.at(i).theta; // estpose.theta = normalize(estpose.theta);
	}

		
	return normalizePose(estpose);
}

/*
 * Compute the angle difference between two angles, ensuring result is normalized. Taken from ROS AMCL
*/	
double angleDiff(double a, double b)
{
  
  a = normalize(a);
  b = normalize(b);


  double d1 = a - b, d2 = 2 * M_PI - fabs(d1);
  
  if(d1 > 0)
    d2 *= -1.0;
  
  if(fabs(d1) < fabs(d2))
    return(d1);
  else
    return(d2);
}


/**
* @desc Computes sample poses from sensor observations of a landmark (=marker)
* @desc From Table 6.5 of Thrun
* @params obs (= observation of marker), marker (=marker location), robotParams (noise Params of the sensor)
* @return Random pose that corresponds to observation 
*/
RobotState sampleLandmarkPose(MarkerObservation obs, FieldLocation marker, RobotParams robotParams)
{

	double gamma = genUniRV() * 2 * M_PI;	
	double r_meas = obs.distance;
	double phi_meas = obs.orientation;
	 

	double r_err_var = robotParams.sensor_noise_distance  * r_meas, r_err_sd = sqrt(r_err_var);
	double r_samp = r_meas + genNormalRV(0,r_err_sd);	
	// std::cout<<"\nr_meas= "<< r_meas <<"\tr_samp=" << r_samp;

	double phi_err_var = robotParams.sensor_noise_orientation, phi_err_sd = sqrt(phi_err_var);
	double phi_samp = phi_meas + genNormalRV(0, phi_err_sd);
	// std::cout<<"\nphi_meas = "<< phi_meas <<"\tphi_samp = "<< phi_samp;

	
	RobotState pose_samp;
	pose_samp.x = marker.x + r_samp * cos(gamma);
	pose_samp.y = marker.y + r_samp * sin(gamma);
	pose_samp.theta = normalize(gamma - M_PI - phi_samp);

	return normalizePose(pose_samp);	
}


/**
* @desc Find the probability (or weight)  of a pose from a given sensor reading
* @desc From Table 6.4 of Thrun
* @params pose = pose of robot,  obs = observation of marker, marker = marker location, robotParams = noise Params of the sensor
* @return probability of pose
*/
double findMeasWeight(RobotState pose, MarkerObservation obs, FieldLocation marker, RobotParams robotParams)
{

	//std::cout << "\nLandmark index = " << obsvec[0].markerIndex <<" known to be at " <<   marker.x << ", " << marker.y;
	//std::cout << "\nMeasurement from position : " << obs.distance << ", " << obs.orientation;
	//std::cout << "\nMy particle is at " << pose.x << "," << pose.y <<"," << pose.theta;
		
	double dx = (pose.x - marker.x), dy = pose.y - marker.y;
	double r_est = sqrt(dx * dx + dy * dy);
	double theta_est = atan2(dy, dx);

	double r_error_var = robotParams.sensor_noise_distance  * r_est, r_error_sd = sqrt(r_error_var);
	double r_prob = findProbNormal(obs.distance - r_est,0, r_error_sd);
				
	double theta_error_var = robotParams.sensor_noise_orientation;
	double theta_prob = findProbNormal( (obs.orientation - theta_est),0,sqrt(theta_error_var));	
	
	//std::cout << "\nr_est = "<< r_est << "\t theta_est = "<< theta_est;	
	//std::cout<<"\nr_prob=" << r_prob << "\t theta_prob=" << theta_prob;

	return theta_prob * r_prob;

}

#endif

